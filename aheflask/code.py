# Code for Flask App

AUTHOR_NAME:str = "Mateusz ZIELINSKI"

def credentials_string(msg: str) -> str:
    return str(f"(c) {msg}")
